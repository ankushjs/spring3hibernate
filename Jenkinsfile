pipeline {
    agent any
    parameters {
        string(name: 'AWS_ACCOUNT_ID', defaultValue: '899758493669', description: 'AWS ACCOUNT ID')
        string(name: 'AWS_REGION', defaultValue: 'us-east-1', description: 'AWS REGION')
        string(name: 'ECS_CLUSTER_NAME', defaultValue: 'demo-ecs-cluster', description: 'ECS CLUSTER NAME')
        string(name: 'ECS_SERVICE_NAME', defaultValue: 'demo-ecs-service', description: 'ECS SERVICE NAME')
    }
    stages {
        stage('build image') {
            steps {
                echo "Building docker image and pushing to ECR!"
                sh '''
                    export AWS_PROFILE=ecs_user
		    echo "${AWS_ACCOUNT_ID}"
	            dockerRegistry="${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com"
                    docker build -t ${dockerRegistry}/opstree-demo-repo:${BUILD_NUMBER} -f Dockerfile .
                    eval "$(aws ecr get-login --no-include-email)"
                '''
            }
        }
        stage('Update Service') {
            steps {
                echo "Using aws cli to Register New Task Definition and Update Revision in Service"
                sh '''
                    export AWS_PROFILE=ecs_user
                    clusterName=${ECS_CLUSTER_NAME}
                    serviceName=${ECS_SERVICE_NAME}
                    maximumPercent=100
                    minimumHealthyPercent=0
                    taskDefArn="$(aws ecs describe-services --cluster ${clusterName} --services ${serviceName} --query services[].taskDefinition --output text)"
                    familyPrefix="$(aws ecs describe-task-definition --task-definition ${taskDefArn} --query taskDefinition.family --output text)"
                    containerDefinition="$(aws ecs describe-task-definition --task-definition ${taskDefArn} --query taskDefinition.containerDefinitions --output json)"
                    previousImageTag="$(aws ecs describe-task-definition --task-definition ${taskDefArn} --query taskDefinition.containerDefinitions[].image --output text | tr '\t' '\n' | cut -d':' -f2 | uniq)"
                    newContainerDefinition="$(echo ${containerDefinition} | sed "s|:${previousImageTag}|:${DOCKER_IMAGE_TAG}|g")"
                    newTaskDefinitionArn=$(aws ecs register-task-definition \
                    --family "${familyPrefix}" \
                    --container-definitions "${newContainerDefinition}" \
                    --query taskDefinition.taskDefinitionArn)
                    newTaskDefinitionArn=`sed -e 's/^"//' -e 's/"$//' <<<"${newTaskDefinitionArn}"`
                    aws ecs update-service --cluster "${clusterName}" --service "${serviceName}" \
                    --task-definition "${newTaskDefinitionArn}" \
                    --deployment-configuration maximumPercent="${maximumPercent}",minimumHealthyPercent="${minimumHealthyPercent}" \
                    --health-check-grace-period-seconds 300
                '''
            }
        }
    }
}
